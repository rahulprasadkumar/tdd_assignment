from dataclasses import dataclass, replace


@dataclass(frozen=True)
class Rover(object):

    facing: str

    def go(self, instruction):
        face = ['N', 'E', 'S', 'W']
        index=face.index(self.facing)
        i = 1
        if instruction == 'L':
            i = -1
        return replace(self, facing=face[(index+i) % 4])