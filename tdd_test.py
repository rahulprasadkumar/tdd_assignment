import unittest
from read_file import Read
from tdd_main import Rover
from unittest import mock

input = Read("input.txt").readFormFile()

class SpaceRoverTest(unittest.TestCase):

    def test_turn_right_N_to_E(self):
        rover=Rover('N')
        rover=rover.go('R')
        self.assertEqual('E',rover.facing)

    def test_turn_right_E_to_S(self):
        rover = Rover('E')
        rover = rover.go('R')
        self.assertEqual('S',rover.facing)

    def test_turn_right_S_to_W(self):
        rover=Rover('S')
        rover=rover.go('R')
        self.assertEqual('W',rover.facing)

    def test_turn_right_W_to_N(self):
        rover=Rover('W')
        rover=rover.go('R')
        self.assertEqual('N',rover.facing)

    def test_turn_left_N_to_W(self):
        rover = Rover('N')
        rover = rover.go('L')
        self.assertEqual('W', rover.facing)

    def test_turn_left_W_to_S(self):
        rover = Rover('W')
        rover = rover.go('L')
        self.assertEqual('S', rover.facing)

    def test_turn_left_S_to_E(self):
        rover = Rover('S')
        rover = rover.go('L')
        self.assertEqual('E', rover.facing)

    def test_turn_left_E_to_N(self):
        rover = Rover('E')
        rover = rover.go('L')
        self.assertEqual('N', rover.facing)

    def test_move_clockwise(self):
        rover = Rover(input[0].split(",")[0])
        for i in range(0,4):
            rover = rover.go(input[i].split(",")[1])
            self.assertEqual(input[i].split(",")[2],rover.facing)

    def test_move_anti_clockwise(self):
        rover = Rover(input[4].split(",")[0])
        for i in range(4,8):
            rover = rover.go(input[i].split(",")[1])
            self.assertEqual(input[i].split(",")[2],rover.facing)
    
    
    # mocking the main file
    @mock.patch("tdd_main.Rover.go")
    def test_mock_right(self, mock_go_1):
        mock_go_1.return_value='E'
        rover = Rover('N')
        assert Rover.go('R') == "E"

    @mock.patch("tdd_main.Rover.go")
    def test_mock_left(self, mock_go_2):
        mock_go_2.return_value='W'
        rover = Rover('N')
        assert Rover.go('L') == 'W'


if __name__ == '__main__':
    unittest.main()