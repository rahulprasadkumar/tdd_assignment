from dataclasses import dataclass
import os
@dataclass
class Read(object):
    file:str
    def readFormFile(self):
        infile=open(self.file, "r")
        line=infile.readlines()
        return line